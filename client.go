package main

import (
	"log"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "test_counter",
		Help: "A histogram of the HTTP request durations in seconds.",
	}, []string{"code"})

	// Create non-global registry.
	registry := prometheus.NewRegistry()

	// Add go runtime metrics and process collectors.
	registry.MustRegister(
		// collectors.NewGoCollector(),
		// collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}),
		counter,
	)

	go func() {
		for {
			counter.WithLabelValues("200").Inc()
			counter.WithLabelValues("500").Inc()
			time.Sleep(600 * time.Millisecond)
		}
	}()

	// Expose /metrics HTTP endpoint using the created custom registry.
	http.Handle(
		"/metrics", promhttp.HandlerFor(
			registry,
			promhttp.HandlerOpts{
				EnableOpenMetrics: true,
			}),
	)

	log.Fatalln(http.ListenAndServe(":4000", nil))
}

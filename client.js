const express = require("express");
const server = express();
const prom = require("prom-client");
const register = prom.register;

//default buckets
prom.collectDefaultMetrics({ gcDurationBuckets: [0.001, 0.01, 0.1, 1, 2, 5] });

const counter = new prom.Counter({
  name: "test_counter",
  help: "Example counter",
  labelNames: ["code"],
});

//inc counter each 10 sec
setInterval(() => {
  counter.inc({ code: 200 });
  counter.inc({ code: 500 });
}, 1000 * 10);

// Setup prometheus scrapes
server.get("/metrics", async (req, res) => {
  console.log("/metrics");
  try {
    res.set("Content-Type", register.contentType);
    res.end(await register.metrics());
  } catch (error) {
    res.status(500).end(error);
  }
});

const port = 4000;
server.listen(port);

# Grafana + prometheus + docker

This project is to create and test metrics locally and make sure that everything is working as expected. Please take a look on Nodejs and Go client examples

## Requirement

- [Docker](https://www.docker.com/)
- [Colima](https://github.com/abiosoft/colima)
- [Golang version 1.18.9+](https://go.dev/dl/) Only for Go client
- [Nodejs](https://nodejs.org/en/) Only for Nodejs client

```bash
brew install docker
brew install colima
brew install golang
brew install node
```

## Usage

```bash
docker run --rm --name prom -p 9090:9090 -v "$PWD"/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
docker run --rm --name grafana -p 3000:3000 --link prom:prom grafana/grafana

#only if you are using node client to generate metrics
npm install
node client.js

#only if you are using go client to generate metrics
go get github.com/prometheus/client_golang/prometheus
go run client.go

#only if you are using java spring client to generate metrics
cd java-demo
./gradlew bootRun
```

Go to http://localhost:3000 user `admin`, pass `admin`. Configure grafana adding a new datasource and setting it up to `http://prom:9090`

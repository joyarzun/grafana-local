package com.example.java_demo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
    private final CustomMetricsService customMetricsService;
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	public ScheduledTasks(CustomMetricsService customMetricsService) {
        this.customMetricsService = customMetricsService;
    }

    @Scheduled(fixedRate = 10000)
	public void reportCurrentTime() {
		log.info("Increment custom counter at {}", dateFormat.format(new Date()));
        customMetricsService.incrementCustomMetric();
	}
}
